﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zfwpg
{
    public partial class Form1 : Form
    {
        Bitmap bitmap;
        Graphics graphics;
        int plotPadding = 25;
        int zoom = 50;
        public Form1()
        {
            InitializeComponent();
            Width += 1;
            bitmap = new Bitmap(pbDrawingArea.Width, pbDrawingArea.Height);
            graphics = Graphics.FromImage(bitmap);
            clearPlot();
            drawAxes();
            drawPlot();
            pbDrawingArea.Image = bitmap;
        }

        private void clearPlot()
        {
            graphics.Clear(Color.White);
            pbDrawingArea.Image = bitmap;
        }

        private void drawAxes()
        {
            Pen mainAxesPen = new Pen(Brushes.Black);
            Pen additionalAxesPen = new Pen(Brushes.Gray);
            graphics.DrawLine(mainAxesPen, plotPadding, bitmap.Height - plotPadding, bitmap.Width - plotPadding, bitmap.Height - plotPadding);
            graphics.DrawLine(mainAxesPen, plotPadding, plotPadding, plotPadding, bitmap.Height - plotPadding);
            int lastShift = 0;
            for (int shift = zoom + plotPadding; shift < bitmap.Width - plotPadding; shift += zoom)
            {
                if (shift - lastShift > 50)
                {
                    lastShift = shift;
                    graphics.DrawLine(additionalAxesPen, shift, plotPadding, shift, bitmap.Height - plotPadding);
                    graphics.DrawString(((shift - plotPadding) / zoom).ToString(), new Font(FontFamily.GenericMonospace, 10), Brushes.Black, shift - 7, bitmap.Height - plotPadding);
                }
            }
            lastShift = bitmap.Height - plotPadding;
            for (int shift = bitmap.Height - plotPadding - zoom; shift > plotPadding; shift -= zoom)
            {
                if (lastShift-shift > 30)
                {
                    lastShift = shift;
                    graphics.DrawLine(additionalAxesPen, plotPadding, shift, bitmap.Width - plotPadding, shift);
                    int value = ((bitmap.Height - plotPadding *2) / zoom - (shift - plotPadding) / zoom);
                    graphics.DrawString(value.ToString(), new Font(FontFamily.GenericMonospace, 10), Brushes.Black, 0, shift - 5);
                }
            }

            graphics.DrawString("h[m]", new Font(FontFamily.GenericMonospace, 10), Brushes.Black, 0, 0);
            graphics.DrawString("s[m]", new Font(FontFamily.GenericMonospace, 10), Brushes.Black, bitmap.Width-40, bitmap.Height - plotPadding);
            pbDrawingArea.Image = bitmap;
        }

        private void drawPlot()
        {
            try
            {
                double v0 = Convert.ToDouble(tbV0.Text.ToString());
                if (v0 < 0 || v0 > 1000)
                {
                    v0 = 0;
                    MessageBox.Show("Niewłaściwy zakres wartości dla prędkości początkowej.\nProszę podać liczbę z przedziału 0 - 1000");
                }
                double height = Convert.ToDouble(tbH.Text.ToString());
                if (height < 0 || height > 600)
                {
                    height = 0;
                    MessageBox.Show("Niewłaściwy zakres wartości dla wysokości początkowej.\nProszę podać liczbę z przedziału 0 - 600");
                }
                double alpha = Convert.ToDouble(tbAlpha.Text.ToString());
                if (alpha < 0 || alpha > 90)
                {
                    alpha = 0;
                    MessageBox.Show("Niewłaściwy zakres wartości dla kąta początkowego.\nProszę podać liczbę z przedziału 0 - 90");
                }
                double g = Convert.ToDouble(tbG.Text.ToString());
                if (g < 0 || g > 1000)
                {
                    g = 0;
                    MessageBox.Show("Niewłaściwy zakres wartości dla przyspieszenia grawitacyjnego.\nProszę podać liczbę z przedziału 0 - 1000");
                }
                

                List<Point> points = new List<Point>();
                for (double t = 0; t < 10; t += 0.05)
                {
                    int x = (int)((v0 * t * Math.Cos(Math.PI * alpha / 180)) * zoom * 2) + plotPadding;
                    int y = (int)((v0 * t * Math.Sin(Math.PI * alpha / 180) - g * t * t / 2) * zoom * 2 + height * zoom) + plotPadding;

                    Point point = new Point(x, bitmap.Height - y);
                    points.Add(point);
                }
                foreach (Point point in points)
                {
                    try
                    {
                        graphics.DrawLine(new Pen(Brushes.Red), point, points.ElementAt(points.IndexOf(point) + 1));
                    }
                    catch (ArgumentOutOfRangeException)
                    {

                    }
                }
                pbDrawingArea.Image = bitmap;
            }
            catch (FormatException)
            {
                MessageBox.Show("Podano nieprawidłowe dane wejściowe.");
            }
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            clearPlot();
            drawAxes();
            drawPlot();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            try
            {
                groupBox1.Location = new Point(Width - groupBox1.Width - 30, 50);
                btnHide.Location = new Point(Width - btnHide.Width - 30, 20);
                pbDrawingArea.Width = Width - 35;
                pbDrawingArea.Height = Height - 55;
                bitmap = new Bitmap(pbDrawingArea.Width, pbDrawingArea.Height);
                graphics = Graphics.FromImage(bitmap);
                clearPlot();
                drawAxes();
                drawPlot();
                pbDrawingArea.Image = bitmap;
            }
            catch (ArgumentException){}
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            if (groupBox1.Visible == true)
            {
                groupBox1.Visible = false;
                btnHide.Text = "Pokaż";
            }
            else
            {
                groupBox1.Visible = true;
                btnHide.Text = "Ukryj";
            }
            
        }

        
        private void btnZoomMinus_Click(object sender, EventArgs e)
        {
            if (zoom > 1)
            {
                zoom -= 1;
                clearPlot();
                drawAxes();
                drawPlot();
                pbDrawingArea.Image = bitmap;
            }
        }

        private void btnZoomPlus_Click(object sender, EventArgs e)
        {
            zoom += 1;
            clearPlot();
            drawAxes();
            drawPlot();
            pbDrawingArea.Image = bitmap;
        }
    }
}
