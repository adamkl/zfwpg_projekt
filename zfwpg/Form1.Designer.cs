﻿namespace zfwpg
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbDrawingArea = new System.Windows.Forms.PictureBox();
            this.tbV0 = new System.Windows.Forms.TextBox();
            this.tbAlpha = new System.Windows.Forms.TextBox();
            this.tbG = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.tbH = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnZoomPlus = new System.Windows.Forms.Button();
            this.btnZoomMinus = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawingArea)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbDrawingArea
            // 
            this.pbDrawingArea.Location = new System.Drawing.Point(12, 12);
            this.pbDrawingArea.Name = "pbDrawingArea";
            this.pbDrawingArea.Size = new System.Drawing.Size(921, 597);
            this.pbDrawingArea.TabIndex = 0;
            this.pbDrawingArea.TabStop = false;
            // 
            // tbV0
            // 
            this.tbV0.Location = new System.Drawing.Point(212, 19);
            this.tbV0.Name = "tbV0";
            this.tbV0.Size = new System.Drawing.Size(100, 26);
            this.tbV0.TabIndex = 2;
            this.tbV0.Text = "10";
            // 
            // tbAlpha
            // 
            this.tbAlpha.Location = new System.Drawing.Point(212, 83);
            this.tbAlpha.Name = "tbAlpha";
            this.tbAlpha.Size = new System.Drawing.Size(100, 26);
            this.tbAlpha.TabIndex = 4;
            this.tbAlpha.Text = "45";
            // 
            // tbG
            // 
            this.tbG.Location = new System.Drawing.Point(212, 115);
            this.tbG.Name = "tbG";
            this.tbG.Size = new System.Drawing.Size(100, 26);
            this.tbG.TabIndex = 5;
            this.tbG.Text = "9,81";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Prędkość początkowa:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(165, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Kąt: ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Przyspiszenie grawitacyjne:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnZoomMinus);
            this.groupBox1.Controls.Add(this.btnZoomPlus);
            this.groupBox1.Controls.Add(this.tbH);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnCalculate);
            this.groupBox1.Controls.Add(this.tbV0);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbAlpha);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbG);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(587, 67);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(329, 239);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(10, 191);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(302, 40);
            this.btnCalculate.TabIndex = 8;
            this.btnCalculate.Text = "Przelicz";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // btnHide
            // 
            this.btnHide.Location = new System.Drawing.Point(833, 23);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(83, 40);
            this.btnHide.TabIndex = 1;
            this.btnHide.Text = "Ukryj";
            this.btnHide.UseVisualStyleBackColor = true;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // tbH
            // 
            this.tbH.Location = new System.Drawing.Point(212, 51);
            this.tbH.Name = "tbH";
            this.tbH.Size = new System.Drawing.Size(100, 26);
            this.tbH.TabIndex = 3;
            this.tbH.Text = "5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "Wysokość początkowa:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnZoomPlus
            // 
            this.btnZoomPlus.Location = new System.Drawing.Point(167, 147);
            this.btnZoomPlus.Name = "btnZoomPlus";
            this.btnZoomPlus.Size = new System.Drawing.Size(145, 40);
            this.btnZoomPlus.TabIndex = 7;
            this.btnZoomPlus.Text = "Przybliż";
            this.btnZoomPlus.UseVisualStyleBackColor = true;
            this.btnZoomPlus.Click += new System.EventHandler(this.btnZoomPlus_Click);
            // 
            // btnZoomMinus
            // 
            this.btnZoomMinus.Location = new System.Drawing.Point(11, 147);
            this.btnZoomMinus.Name = "btnZoomMinus";
            this.btnZoomMinus.Size = new System.Drawing.Size(145, 40);
            this.btnZoomMinus.TabIndex = 6;
            this.btnZoomMinus.Text = "Oddal";
            this.btnZoomMinus.UseVisualStyleBackColor = true;
            this.btnZoomMinus.Click += new System.EventHandler(this.btnZoomMinus_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 621);
            this.Controls.Add(this.btnHide);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pbDrawingArea);
            this.Name = "Form1";
            this.Text = "Rzut Ukośny";
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawingArea)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbDrawingArea;
        private System.Windows.Forms.TextBox tbV0;
        private System.Windows.Forms.TextBox tbAlpha;
        private System.Windows.Forms.TextBox tbG;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.TextBox tbH;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnZoomMinus;
        private System.Windows.Forms.Button btnZoomPlus;
    }
}

